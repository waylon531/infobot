use core::cmp::Ordering;
use std::fmt;
use std::str;
use serde::{Serialize, Deserialize};
use comrak::{Arena, parse_document, format_html, ComrakOptions};


#[derive(Debug,PartialEq,Eq,Serialize,Deserialize,Clone)]
pub struct Fact {
    pub count: u64,
    pub contents: String
}

impl Fact {
    pub fn new(contents: String) -> Fact {
        Fact {
            contents,
            count: 1
        }
    }
}

impl PartialOrd for Fact {
    fn partial_cmp(&self, other: &Fact) -> Option<Ordering> {
        Some(other.count.cmp(&self.count))
    }
}

impl Ord for Fact {
    fn cmp(&self, other: &Fact) -> Ordering {
        other.count.cmp(&self.count)
    }
}

#[derive(Debug,PartialEq,Serialize,Deserialize,Clone)]
pub struct Facts {
    contents: Vec<Fact>
}

impl Facts {
    pub fn new() -> Facts {
        Facts {
            contents: Vec::new()
        }
    }
    pub fn render_facts(&mut self) {
        for Fact { count: _, ref mut contents } in self.contents.iter_mut() {
            let arena = Arena::new();

            let root = parse_document(
                &arena,
                &contents,
                &ComrakOptions::default());
            let mut html = vec![];

            // UNWRAP BOYS LETS GO
            format_html(root, &ComrakOptions::default(), &mut html).unwrap();
            *contents = str::from_utf8(&html)
                .unwrap()
                .trim_end_matches("</p>\n")
                .trim_start_matches("<p>")
                .to_string();
        }


    }
    pub fn insert<S: Into<String>>(&mut self,fact: S) {
        self.add_fact(Fact::new(fact.into()));
    }
    pub fn add_fact(&mut self,fact: Fact) {
        // If we should add to the count of an existing entry
        if let Some(position) = self.contents.iter().position(|x| x.contents == fact.contents) {
            //Then add to the entry
            self.contents[position].count += 1;
            
            //And re-sort the list
            self.contents.sort_unstable()

        // Or create a new one
        } else {
            self.contents.push(fact);
        }
    }
    pub fn from_bytes(bytes: &[u8]) -> Option<Facts> {
        serde_json::from_slice(bytes).ok()
    }
    pub fn to_bytes(&self) -> Vec<u8> {
        // This probably shouldn't fail
        serde_json::to_vec(self).expect("This shouldn't fail")
    }
}

impl fmt::Display for Fact {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.count == 1 {
            write!(f, "{}", self.contents)
        } else {
            write!(f, "{} (x{})", self.contents, self.count)
        }
    }
}
impl fmt::Display for Facts {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.contents.len() == 0 {
            return write!(f, "[]");
        }
        let mut fact_iter = self.contents.iter();
        let first_fact = fact_iter.next().unwrap();
        write!(f, "{}", first_fact)?;
        for fact in fact_iter {
            write!(f, " or {}", fact)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::{Fact,Facts};
    #[test]
    fn ordered_insert() {
        // Insert a single fact 20 times
        let f = Fact::new("++".to_string());
        let mut facts = Facts::new();
        facts.insert("--");
        for _ in 0..5 {
            facts.add_fact(f.clone());
        }

        // Create a big fact with count 20
        let mut big_fact = Fact::new("++".to_string());
        big_fact.count=5;
        let mut other_facts = Facts::new();
        other_facts.add_fact(big_fact);
        other_facts.insert("--");
        assert_eq!(facts,other_facts);
    }
    #[test]
    fn multi_insert() {
        // Insert a single fact 20 times
        let f = Fact::new("++".to_string());
        let mut facts = Facts::new();
        for _ in 0..20 {
            facts.add_fact(f.clone());
        }
        // Create a big fact with count 20
        let mut big_fact = Fact::new("++".to_string());
        big_fact.count=20;
        let mut other_facts = Facts::new();
        other_facts.add_fact(big_fact);
        assert_eq!(facts,other_facts);
    }
}
